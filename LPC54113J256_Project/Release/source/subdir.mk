################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../source/EEPROM.cpp \
../source/LPC54113J256_Project.cpp \
../source/PowerButton.cpp \
../source/SPI.cpp \
../source/USART.cpp \
../source/cpp_config.cpp 

C_SRCS += \
../source/semihost_hardfault.c 

OBJS += \
./source/EEPROM.o \
./source/LPC54113J256_Project.o \
./source/PowerButton.o \
./source/SPI.o \
./source/USART.o \
./source/cpp_config.o \
./source/semihost_hardfault.o 

CPP_DEPS += \
./source/EEPROM.d \
./source/LPC54113J256_Project.d \
./source/PowerButton.d \
./source/SPI.d \
./source/USART.d \
./source/cpp_config.d 

C_DEPS += \
./source/semihost_hardfault.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DNDEBUG -D__NEWLIB__ -DFSL_SDK_DISABLE_DRIVER_RESET_CONTROL=1 -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\drivers" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\CMSIS" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\device" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\drivers" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\CMSIS" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\device" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\board" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\source" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\startup" -Os -fno-common -g -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fno-rtti -fno-exceptions -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DCPU_LPC54113J256BD64 -DCPU_LPC54113J256BD64_cm4 -D__LPC5411X__ -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DNDEBUG -D__NEWLIB__ -DFSL_SDK_DISABLE_DRIVER_RESET_CONTROL=1 -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\drivers" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\CMSIS" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\device" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\drivers" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\CMSIS" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\device" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\board" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\source" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project" -I"C:\Users\Matthijs\Documents\MCUExpressoWorkspaces\Pedle\pedlecontrollerboard\LPC54113J256_Project\startup" -Os -fno-common -g -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


