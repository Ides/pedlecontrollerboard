/*
 * USART.h
 *
 *  Created on: 1 Nov 2019
 *      Author: Matthijs
 */

#ifndef USART_H_
#define USART_H_

#include "fsl_usart.h"

#define RECEIVE_SIZE 1

class USART
{
private:
	// Store the usart base
	USART_Type *mBase;

	// The handle to the usart instance
	usart_handle_t mUsartHandle;

	// Transfer structure for receiving
	usart_transfer_t mReceiveTransfer;

	// Buffer for receiving
	uint8_t mReceiveBuffer[RECEIVE_SIZE];

	// Size of the receive buffer
	uint8_t mReceiveBufferSize = RECEIVE_SIZE;

	// Transfer structure for Sending
	usart_transfer_t mTransmitTransfer;

	uint8_t mTransmitBuffer[256];

	// Make a ring buffer of 256 bytes so we can use the overflow to keep reading/writing
	uint8_t mReceiveRingBuffer[256];
	uint8_t mReceiveRingBufferWriteIndex = 0;
	uint8_t mReceiveRingBufferReadIndex = 0;

	bool mTxBusy = false;

	// Callback for the uasrt transfers
	static void USART_UserCallback(USART_Type *base, usart_handle_t *handle, status_t status, void *userData);
public:
	// Constructor
	USART(USART_Type *base, uint32_t flexcommFreq);
	// Destructor
	virtual ~USART();

	bool TryGetMessage(uint8_t *buffer, uint8_t bufferLength, uint8_t *receivedMessageLength);
	status_t TrySendMessage(uint8_t *buffer, uint8_t length);

};

#endif /* USART_H_*/
