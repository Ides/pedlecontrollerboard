#ifndef _EEPROM_H_
#define _EEPROM_H_

#include <stdint.h>
#include <stdio.h>
#include "fsl_spi.h"
#include "SPI.h"

class EEPROM
{
private:
	spi_ssel_t _ssel;
	SPI *_spi;

	int ReadStateReg(uint8_t *StateReg);
	void EnableWREN(void);
	void WaitForWIP(void);
public:
	// Constructor
	EEPROM(SPI *spi_master, spi_ssel_t ssel);
	// Destructor
	virtual ~EEPROM();

	int Write(uint32_t Address, uint8_t *tx_Buffer, size_t length);
	int Read(uint32_t Address, uint8_t *rx_Buffer, size_t length);
	int Read(uint32_t Address, uint16_t *Buf);
	int Read(uint32_t Address, uint32_t *Buf);
};


#endif /* _EEPROM_H_ */
