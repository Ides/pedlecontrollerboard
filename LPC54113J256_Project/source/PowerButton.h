#ifndef _POWER_BUTTON_H_
#define _POWER_BUTTON_H_

#include "fsl_common.h"
#include "fsl_ctimer.h"
#include "fsl_gpio.h"
#include "fsl_pint.h"

struct PinDefinition
{
	GPIO_Type *base;
	uint32_t port;
	uint32_t pin;
};

typedef void (*button_action_callback)(uint8_t DetectedActionType);

class PowerButton
{
private:
	PinDefinition SwitchPinDefinition;
	PinDefinition LEDPinDefinition;

	uint32_t ButtonUpTimeStamp;
	uint32_t ButtonDownTimeStamp;
	bool ButtonPressed;
	bool PreviousButtonState;
	// Wil be set when a long press is detected, reset when up-event is detected
	bool LongPressDetected;

	// Used to indicate the button was still pressed when we started. This prevents the later button_up event to trigger a shutdown...
	bool StartupPressCycleActive;

	void HandleButtonDownEvent(void);
	void HandleButtonUpEvent(void);

	void HandleShortPressDetect(void);
	void HandleLongPressDetect(void);

	bool ReadButtonState(void);
	uint32_t GetPinState(PinDefinition pin);

	button_action_callback ButtonActionCallback;

	CTIMER_Type* TimerBase;

public:
	// Constructor
	PowerButton(PinDefinition switchPinDefinition, PinDefinition ledPinDefinition, button_action_callback actionCallback, CTIMER_Type* timerBase);
	// Destructor
	virtual ~PowerButton();

	// Call this function to keep everything running!
	void MainLoop(void);

	// Returns the current state of the button. True when pressed; false when released
	bool GetPressedState(void);
	// Call this if there is a change detected on the push button pin
	//void SwitchStateChanged(void);


	// This function will set the push button led to the state provided (true = on).
	// NOTE: State will be set/reset after a button push/release!
	void SetLed(bool setOn);

	// Returns the time since the last button release (or 0 when still pressed) in milliseconds
	uint32_t GetButtonIdleTime(void);
};

#endif /* _DISPLAY_MASTER_H_ */
