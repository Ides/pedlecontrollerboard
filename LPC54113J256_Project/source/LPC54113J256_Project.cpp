/*
 * Copyright 2016-2020 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    LPC54113J256_Project.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC54113.h"
/* TODO: insert other include files here. */

#include "fsl_flexcomm.h"
#include "fsl_gpio.h"
#include "fsl_inputmux.h"
#include "fsl_pint.h"
#include "fsl_clock.h"
#include "fsl_power.h"

#include "PowerButton.h"
#include "USART.h"
#include "SPI.h"
#include "EEPROM.h"
/* TODO: insert other definitions and declarations here. */


#define SoftwareVersionMajor	0
#define SoftwareVersionMinor	2

#define ReadCommandID			0xA0
#define WriteCommandID			0xA1
#define NegativeResponseID		0x7F

#define NR_FunctionNotSupported	0x11
#define NR_ParamNotSupported	0x12
#define NR_RequestOutOfRange	0x31

#define SERIAL_LENGTH			40

#define EEPROM_LOCATION_SERIAL	0x00000000

PowerButton *powerButton;
bool RaspberryPiBootDetected = false;
USART *RaspberryPiUSART;
bool ShortPressDetected = false;

// The boot state of the Raspberry Pi. We assume it to be off at start-up
bool RPIBootedState = false;
CTIMER_Type* TimerBase;

bool shutdownPending = false;



SPI *spi_fc0;
EEPROM *eeprom;


void SPI_GetEEPROMConfig(spi_master_config_t *config)
{
    assert(NULL != config);

    /* Initializes the configure structure to zero. */
    memset(config, 0, sizeof(*config));

    config->enableLoopback = false;
    config->enableMaster = true;
    config->polarity = kSPI_ClockPolarityActiveHigh;
    config->phase = kSPI_ClockPhaseFirstEdge;
    config->direction = kSPI_MsbFirst;
    config->baudRate_Bps = 5000000;
    config->dataWidth = kSPI_Data8Bits;
    config->sselNum = kSPI_Ssel0;
    config->txWatermark = kSPI_TxFifo0;
    config->rxWatermark = kSPI_RxFifo1;
    config->sselPol = kSPI_SpolActiveAllLow;
    config->delayConfig.preDelay = 0U;
    config->delayConfig.postDelay = 0U;
    config->delayConfig.frameDelay = 0U;
    config->delayConfig.transferDelay = 0U;
}



void SetupTimer()
{
    ctimer_config_t config;
    CTIMER_GetDefaultConfig(&config);

    // Set the prescaler so we end up with ticks of 1 ms
    //config.prescale = Fclock/1000;
    config.prescale = 48000000/1000;

    // Init the timer
    CTIMER_Init(TimerBase, &config);

    // Start the timer
    CTIMER_StartTimer(TimerBase);
}

uint32_t GetTimerTick()
{
	return CTIMER_GetTimerCountValue(TimerBase);
}

void KillMainSupply(void)
{
	// Make sure the button is released before we shutdown....
	while(powerButton->GetPressedState())
	{
		powerButton->MainLoop();
	}

	// Then, kill the main power supply!
	GPIO_PinWrite(
			BOARD_INITPINS_KILL_MAIN_PS_GPIO,
			BOARD_INITPINS_KILL_MAIN_PS_PORT,
			BOARD_INITPINS_KILL_MAIN_PS_PIN,
			1);
}


static void button_callback(uint8_t DetectedActionType)
{
//	printf("Action %d\r\n", DetectedActionType);

	// Short Press Detected
	if(DetectedActionType == 0x01)
	{
		//printf("Short Press Detected...\r\n");
		ShortPressDetected = true;
	}
	// Long Press Detected
	else if(DetectedActionType == 0x02)
	{
		// Set the push button LED to OFF to indicate the user we have reached the long press state
		powerButton->SetLed(false);

		// Go to the kill main supply routine
		KillMainSupply();
	}
}

bool IsRPIRunning(void)
{
	// Return the state of the RPI Detect Input Pin
	return GPIO_PinRead(
			BOARD_INITPINS_RPI_DETECT_INPUT_GPIO,
			BOARD_INITPINS_RPI_DETECT_INPUT_PORT,
			BOARD_INITPINS_RPI_DETECT_INPUT_PIN);
}

void Control5VoltSupply(bool enable)
{
	GPIO_PinWrite(
			BOARD_INITPINS_ENABLE_PI_PS_GPIO,
			BOARD_INITPINS_ENABLE_PI_PS_PORT,
			BOARD_INITPINS_ENABLE_PI_PS_PIN,
			enable);
}

void SendPositiveWriteResponse(uint8_t parameterId)
{
	uint8_t positiveWriteResponseMsg[2];

	// Set the positive write response code
	positiveWriteResponseMsg[0] = WriteCommandID + 0x20;

	// Set the parameter id that was successfully written
	positiveWriteResponseMsg[1] = parameterId;

	// Send the message
	RaspberryPiUSART->TrySendMessage(positiveWriteResponseMsg, 2);
}

void SendNegativeResponse(uint8_t failedFunction, uint8_t failCode)
{
	uint8_t negativeResponseMsg[3];

	// Set the negative response code
	negativeResponseMsg[0] = NegativeResponseID;

	// Set the function that failed
	negativeResponseMsg[1] = failedFunction;

	// Set the fail code
	negativeResponseMsg[2] = failCode;

	// Send the message
	RaspberryPiUSART->TrySendMessage(negativeResponseMsg, 3);
}

void HandleUSARTComm()
{
	uint8_t ReceiveBuffer[64];
	uint8_t ReceivedLength;

	if(RaspberryPiUSART->TryGetMessage(ReceiveBuffer, 64, &ReceivedLength))
	{
		// If the first byte is 0xA1, this a Write Request
		if(ReceiveBuffer[0] == WriteCommandID)
		{
			// We need at least three bytes to have a valid write operation....
			if(ReceivedLength <= 2)
			{
				// Send a request out-of-range response
				SendNegativeResponse(WriteCommandID, NR_RequestOutOfRange);
				return;
			}

			switch(ReceiveBuffer[1])
			{
			// Write Tester Serial
			case 0x01:
			{
				// If we do not have the required bytes of length...
				if(ReceivedLength != SERIAL_LENGTH + 2)
				{
					// ..send the request out of range response
					SendNegativeResponse(WriteCommandID, NR_RequestOutOfRange);
					// ..and return
					return;
				}

				// Write the serial to the EEPROM
				eeprom->Write(EEPROM_LOCATION_SERIAL, &ReceiveBuffer[2], SERIAL_LENGTH);

				SendPositiveWriteResponse(ReceiveBuffer[1]);

				break;
			}

				// Write the shutdown pending value
				case 0x80:
				{
					// If we do not have the required 3 bytes of length...
					if(ReceivedLength != 3)
					{
						// ..send the request out of range response
						SendNegativeResponse(WriteCommandID, NR_RequestOutOfRange);
						// ..and return
						return;
					}

					// Set the shutdown pending to false if zero is received, all other values will set the shutdown pending (don't know why anyone would want to set this value though)
					shutdownPending = ReceiveBuffer[2] == 0x00 ? false : true;

					SendPositiveWriteResponse(ReceiveBuffer[1]);

					break;
				}

				// Write custom bytes to the EEPROM
				case 0xFF:
				{
					// Get the number of bytes to write
					uint16_t writeSize = ReceiveBuffer[2];

					// If we do not have the required bytes of length...
					// We need: writeSize for the actual bytes + 3 bytes of overhead (Command, SubCommand and Length Byte) + 4 bytes for the addres to write to in the EEPROM
					if(ReceivedLength != writeSize + 3 + 4)
					{
						// ..send the request out of range response
						SendNegativeResponse(WriteCommandID, NR_RequestOutOfRange);
						// ..and return
						return;
					}

					// Get the address to write to
					uint32_t writeAddress = ReceiveBuffer[3] << 24;
					writeAddress += ReceiveBuffer[4] << 16;
					writeAddress += ReceiveBuffer[5] << 8;
					writeAddress += ReceiveBuffer[6];

					// Write!
					eeprom->Write(writeAddress, &ReceiveBuffer[7], writeSize);

					SendPositiveWriteResponse(ReceiveBuffer[1]);

					break;
				}

				// If the parameter ID is not found...
				default:
				{
					// ..create and send the parameter not supported message
					SendNegativeResponse(WriteCommandID, NR_ParamNotSupported);
					break;
				}
			}
			//KillMainSupply();
			/*
			CurrentEncoderValue = (ReceiveBuffer[1] * 256) + ReceiveBuffer[2];
			*/
		}
		// If the first byte is 0xA0, this a Read Request
		else if(ReceiveBuffer[0] == ReadCommandID)
		{
			// We need two bytes to have a valid read operation....
			if(ReceivedLength != 2)
			{
				// Send a request out-of-range response
				SendNegativeResponse(ReadCommandID, NR_RequestOutOfRange);
				return;
			}

			switch(ReceiveBuffer[1])
			{
				// Read controller board software version
				case 0x00:
				{
					uint8_t softwareVersionMsg[4];

					// Create the message with the software version
					softwareVersionMsg[0] = ReadCommandID + 0x20;
					softwareVersionMsg[1] = ReceiveBuffer[1];
					softwareVersionMsg[2] = SoftwareVersionMajor;
					softwareVersionMsg[3] = SoftwareVersionMinor;
					RaspberryPiUSART->TrySendMessage(softwareVersionMsg, 4);

					break;
				}

				// Read Serial
				case 0x01:
				{
					uint8_t testSerialMsg[SERIAL_LENGTH + 2];

					eeprom->Read(EEPROM_LOCATION_SERIAL, &testSerialMsg[2], SERIAL_LENGTH);

					// Create the command and sub-command part of the reply
					testSerialMsg[0] = ReadCommandID + 0x20;
					testSerialMsg[1] = ReceiveBuffer[1];

					RaspberryPiUSART->TrySendMessage(testSerialMsg, SERIAL_LENGTH + 2);

					break;
				}

				// Read Shutdown Pending State
				case 0x80:
				{
					uint8_t shutdownPendingMsg[3];

					// Create a message with the shutdown pending state
					shutdownPendingMsg[0] = ReadCommandID + 0x20;
					shutdownPendingMsg[1] = ReceiveBuffer[1];
					shutdownPendingMsg[2] = shutdownPending;
					RaspberryPiUSART->TrySendMessage(shutdownPendingMsg, 3);

					break;
				}

				// If the parameter ID is not found...
				default:
				{
					// ..create and send the parameter not supported message
					SendNegativeResponse(ReadCommandID, NR_ParamNotSupported);
					break;
				}
			}
			/*
			uint16_t tmp = ReceiveBuffer[1] << 8 | ReceiveBuffer[2];
			// Clear all the bits in the short press register
			ButtonShortPressStates &= ~tmp;
			PreviousButtonShortPressStates &= ~tmp;

			// Get the written value to the clear register
			tmp = ReceiveBuffer[3] << 8 | ReceiveBuffer[4];
			// Clear all the bits in the long press register
			ButtonLongPressStates &= ~tmp;
			PreviousButtonLongPressStates &= ~tmp;
			*/
		}
		else
		{
			// Send a function not supported negative response
			SendNegativeResponse(ReceiveBuffer[0], NR_FunctionNotSupported);
		}
	}
}

void CheckRPIState()
{
	// Get the current state of the RPI Running Pin
	bool currentPRIPinState = IsRPIRunning();

	// If the Rpi was not yet booted and is now...
	if(!RPIBootedState && currentPRIPinState)
	{
		// ..we set the booted state to true
		RPIBootedState = true;
	}
	// if the Rpi was booted and is not anymore...
	else if(RPIBootedState && !currentPRIPinState)
	{
		// ..set the booted state to false (bit useless maybe...)
		RPIBootedState = false;

		// NOTE: We wait for another 4 seconds here to make sure the RPI has been shut-down completely

		// Get the current timer tick value
		uint32_t tick = GetTimerTick();
		// Wait for 4 seconds
		while(GetTimerTick() - tick < 4000)
		{
			__asm volatile ("nop");
		}

		// ..and go to the kill power supply routine
		KillMainSupply();
	}

	// If the RPI is still not booted....
	if(!RPIBootedState)
	{
		// ..Check for how long we are waiting.
		// For now, we do this by checking for how long the user has left the power button untouched:

		// If the idle time is longer than 60 seconds...
		if(powerButton->GetButtonIdleTime() >= 60000)
		{
			// ..Go to the kill power supply routine
			KillMainSupply();
		}
	}
}

/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();

    //FLEXCOMM_SetIRQHandler(FLEXCOMM4_PERIPHERAL, &FC4_UASRT_CallBack, NULL);
    // Enable the Raspberry PI also when we start
    Control5VoltSupply(true);

    // Choose a timer to use
    TimerBase = CTIMER0;

    // Setup the timer to have ticks of 1ms
    SetupTimer();

    PinDefinition PB_LedPin;
    PB_LedPin.base = GPIO;
    PB_LedPin.port = 0;
    PB_LedPin.pin = 22;

    PinDefinition PB_SwitchPin;
    PB_SwitchPin.base = GPIO;
    PB_SwitchPin.port = 1;
    PB_SwitchPin.pin = 17;

    // Create an instance of the power button class
    powerButton = new PowerButton(PB_SwitchPin, PB_LedPin, button_callback, TimerBase);

    // Create an instance of the raspberry pi communication USART
    RaspberryPiUSART = new USART(USART4, 48000000);

    // Add the 'FSL_SDK_DISABLE_DRIVER_RESET_CONTROL' with value '1' to the projects 'Paths & Symbols' list to not have the GPIO Setup erased by the init function
    //InitPinInterupts();
    // Moved / Copied this down here again, because the Init Pin Interrupts F's up the Pin settings :/
    //BOARD_InitBootPins();


    // MasterConfig for the spi communication with the eeprom
    spi_master_config_t masterConfig;

    // Get the spi config for the eeprom communication
    SPI_GetEEPROMConfig(&masterConfig);

    // Create the spi class
    spi_fc0 = new SPI(SPI0, &masterConfig);

    // create an instance of the eeprom class
    eeprom = new EEPROM(spi_fc0, kSPI_Ssel0);

//    uint16_t value;
//    eeprom->Read(0x00000000, &value);
//
//    uint8_t writeValue[2];
//    writeValue[0] = 0xAA;
//    writeValue[1] = 0x55;
//    eeprom->Write(0x00000000, writeValue, 2);

    uint32_t LastStateCheckTimeStamp = 0;
    while(1)
    {
    	// Only check the RPI State pin every half second, to always have a stable reading
    	if(GetTimerTick() - LastStateCheckTimeStamp > 500)
    	{
    		LastStateCheckTimeStamp = GetTimerTick();
    		CheckRPIState();
    	}

    	powerButton->MainLoop();

    	HandleUSARTComm();



    	if(ShortPressDetected)
    	{
    		// Set the shutdown pending flag
    		shutdownPending = true;

    		// Clear the flag indicating we have a short press detected
    		ShortPressDetected = false;
    	}

        __asm volatile ("nop");
    }
    return 0 ;
}
