/*
 * I2C.cpp
 *
 *  Created on: 14 Dec 2018
 *      Author: Matthijs
 */

#include <USART.h>
#include <stdio.h>


USART::USART(USART_Type *base, uint32_t flexcommFreq)
{
	// Store the USART Base for reference
	mBase = base;

	// Create a config
	usart_config_t config;

	// Get the deafult config
    USART_GetDefaultConfig(&config);
/*  config->baudRate_Bps = 115200U;
    config->parityMode = kUSART_ParityDisabled;
    config->stopBitCount = kUSART_OneStopBit;
    config->bitCountPerChar = kUSART_8BitsPerChar;
    config->loopback = false;
    config->enableRx = false;
    config->enableTx = false;
    config->txWatermark = kUSART_TxFifo0;
    config->rxWatermark = kUSART_RxFifo1; */

    // Enable Tx and Rx
    config.enableTx = true;
    config.enableRx = true;

    // Init the USART
    USART_Init(mBase, &config, flexcommFreq);

    // Create the transfer handle
    USART_TransferCreateHandle(mBase, &mUsartHandle, USART_UserCallback, this);

    // Set the Receiver data buffer and size
	mReceiveTransfer.data = mReceiveBuffer;
	mReceiveTransfer.dataSize = mReceiveBufferSize;

	// Enable reception of data!
	USART_TransferReceiveNonBlocking(mBase, &mUsartHandle, &mReceiveTransfer, NULL);
}

USART::~USART()
{
	// TODO Auto-generated destructor stub
}

bool USART::TryGetMessage(uint8_t *buffer, uint8_t bufferLength, uint8_t *receivedMessageLength)
{
	if(mReceiveRingBufferReadIndex != mReceiveRingBufferWriteIndex)
	{
		// Define the bytes written since the last decoded message
		uint8_t bytesReadCount = mReceiveRingBufferWriteIndex - mReceiveRingBufferReadIndex;
		uint8_t messageLength = mReceiveRingBuffer[mReceiveRingBufferReadIndex];
		// Do we have enough bytes to decode the message? We add 1 for the length byte
		if(bytesReadCount >= messageLength + 1)
		{
			// Copy all bytes to the provided buffer
			for(uint8_t i = 0; i < messageLength; i++)
			{
				// We add one to the index of the receive buffer to skip the length byte
				buffer[i] = mReceiveRingBuffer[(uint8_t)(mReceiveRingBufferReadIndex + 1 + i)];
			}

			if(receivedMessageLength)
				*receivedMessageLength = messageLength;

			mReceiveRingBufferReadIndex += messageLength + 1;
			return true;
		}
	}

	return false;
}

status_t USART::TrySendMessage(uint8_t *buffer, uint8_t length)
{
	if(length > 255)
		return -1;
	if(mTxBusy)
		return -1;

	// Set the first byte to the length of the message
	mTransmitBuffer[0] = length;
	// Copy the buffer to the transmit buffer (with one offset)
	memcpy(&mTransmitBuffer[1], buffer, length);

	mTransmitTransfer.data = mTransmitBuffer;
	mTransmitTransfer.dataSize = length + 1;

	mTxBusy = true;

	return USART_TransferSendNonBlocking(mBase, &mUsartHandle, &mTransmitTransfer);
}


void USART::USART_UserCallback(USART_Type *base, usart_handle_t *handle, status_t status, void *userData)
{
    if (kStatus_USART_TxIdle == status)
    {
    	((USART*)userData)->mTxBusy = false;
    }

    if (kStatus_USART_RxIdle == status)
    {
    	// Get the character from the recieve buffer
    	uint8_t ch = ((USART*)userData)->mReceiveBuffer[0];

    	// Copy the read byte to the buffer
    	((USART*)userData)->mReceiveRingBuffer[((USART*)userData)->mReceiveRingBufferWriteIndex++] = ch;

    	// Keep on receiving
    	USART_TransferReceiveNonBlocking(base, handle, &((USART*)userData)->mReceiveTransfer, NULL);
    }
}
