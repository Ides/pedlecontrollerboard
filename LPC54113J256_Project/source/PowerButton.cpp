/*
 * PowerButton.cpp
 *
 *  Created on: 22 apr. 2020
 *      Author: Matthijs
 */


#include "PowerButton.h"
#include <stdio.h>

// The long press time in ms
#define LONG_PRESS_TIME			10000

// The minimum time in ms for a press to be detected (as short press)
#define MIN_SHORT_PRESS_TIME	150

PowerButton::PowerButton(PinDefinition switchPinDefinition, PinDefinition ledPinDefinition, button_action_callback actionCallback, CTIMER_Type* timerBase)
{
	SwitchPinDefinition = switchPinDefinition;
	LEDPinDefinition = ledPinDefinition;
	ButtonActionCallback = actionCallback;

	TimerBase = timerBase;

	// Get the clock frequency of the input
	//uint32_t Fclock = CLOCK_GetFreq(kCLOCK_AsyncApbClk);



    // Set the Button Time Stamp values to something useful
    ButtonUpTimeStamp = CTIMER_GetTimerCountValue(TimerBase);
	ButtonDownTimeStamp = CTIMER_GetTimerCountValue(TimerBase);


    // We just started, so we have not yet detected a long press
    LongPressDetected = false;

    // Logic of the input switch is reversed (high reading is idle / off state)
    if(GetPinState(switchPinDefinition) == 1)
    { // Button was already released when we started
    	HandleButtonUpEvent();

    	// No startup press cycle active
    	StartupPressCycleActive = false;
    }
    else
    { // The button was still pressed when we started....
    	HandleButtonDownEvent();

    	// Startup press cycle active
    	StartupPressCycleActive = true;
    }

}

PowerButton::~PowerButton()
{

}

void PowerButton::MainLoop(void)
{
    // Logic of the input switch is reversed (high reading is idle / off state)

	bool readButtonState = ReadButtonState();

	if(readButtonState != PreviousButtonState)
	{
		// If the new state is NOT pressed...
		if(readButtonState == false)
			// ..handle the UP event
			HandleButtonUpEvent();
		// otherwise...
		else
			// ..handle the DOWN event
			HandleButtonDownEvent();
	}

	if(ButtonPressed && !LongPressDetected)
	{
		if(CTIMER_GetTimerCountValue(TimerBase) - ButtonDownTimeStamp >= LONG_PRESS_TIME)
		{
			LongPressDetected = true;
			HandleLongPressDetect();
		}
	}
}
bool PowerButton::GetPressedState(void)
{
	return ButtonPressed;
}

bool PowerButton::ReadButtonState()
{
	return GetPinState(SwitchPinDefinition) ? false : true;
}

uint32_t PowerButton::GetPinState(PinDefinition pin)
{
	return GPIO_PinRead(pin.base, pin.port, pin.pin);
}

void PowerButton::SetLed(bool setOn)
{
	GPIO_PinWrite(LEDPinDefinition.base, LEDPinDefinition.port,LEDPinDefinition.pin, setOn);
}


// What to do when the button was pressed and is now released
void PowerButton::HandleButtonUpEvent(void)
{
	// Set the state of the button
	ButtonPressed = false;

	// Set the 'previous state' of the button
	PreviousButtonState = false;

	// Get the time of the up-event
	ButtonUpTimeStamp = CTIMER_GetTimerCountValue(TimerBase);

	// If we did not yet detected a long press... (AND WE ARE NOT IN THE StartupPressCycleActive)
	if(!LongPressDetected && !StartupPressCycleActive)
	{
		// Check if we did not just received a glitch press
		if(ButtonUpTimeStamp - ButtonDownTimeStamp >= MIN_SHORT_PRESS_TIME)
			// We have detected a short press
			HandleShortPressDetect();
		//printf("%d - %d = %d\r\n", ButtonUpTimeStamp, ButtonDownTimeStamp, ButtonUpTimeStamp - ButtonDownTimeStamp);
	}
	else
		// after a long press, we can send the button up action
		ButtonActionCallback(0x00);

	// Make sure we leave the StartupPressCycleActive
	StartupPressCycleActive = false;


	// Reset the long press detected flag
	LongPressDetected = false;

	// Turn the LED off
	SetLed(false);
}

// What to do when the button is pressed
void PowerButton::HandleButtonDownEvent(void)
{
	// Set the state of the button
	ButtonPressed = true;

	// Set the 'previous state' of the button
	PreviousButtonState = true;

	// Get the time of the down-event
	ButtonDownTimeStamp = CTIMER_GetTimerCountValue(TimerBase);

	// Turn the LED on
	SetLed(true);
}

void PowerButton::HandleShortPressDetect(void)
{
	ButtonActionCallback(0x01);
}

void PowerButton::HandleLongPressDetect(void)
{
	ButtonActionCallback(0x02);
}

uint32_t PowerButton::GetButtonIdleTime()
{
	// If the button is down, we do not have any idle time...
	if(ButtonPressed)
		// ..so return 0
		return 0;
	// else if the button is not pressed...
	else
		// ..we return the difference between now and the up-timestamp
		return CTIMER_GetTimerCountValue(TimerBase) - ButtonUpTimeStamp;
}
