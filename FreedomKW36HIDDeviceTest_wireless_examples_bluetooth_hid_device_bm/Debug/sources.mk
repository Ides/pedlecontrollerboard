################################################################################
# Automatically-generated file. Do not edit!
################################################################################

OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
CMSIS \
bluetooth/hci_transport/source \
bluetooth/host/config \
bluetooth/profiles/battery \
bluetooth/profiles/device_info \
bluetooth/profiles/hid \
board \
drivers \
framework/DCDC/Source/MKW36Z \
framework/Flash/Internal \
framework/FunctionLib \
framework/GPIO \
framework/Keyboard/Source \
framework/LED/Source \
framework/Lists \
framework/MWSCoexistence/Source \
framework/MemManager/Source \
framework/Messaging/Source \
framework/ModuleInfo \
framework/NVM/Source \
framework/OSAbstraction/Source \
framework/Panic/Source \
framework/RNG/Source \
framework/Reset \
framework/SecLib \
framework/SerialManager/Source/I2C_Adapter \
framework/SerialManager/Source/SPI_Adapter \
framework/SerialManager/Source \
framework/SerialManager/Source/UART_Adapter \
framework/TimersManager/Source \
framework/XCVR/MKW36Z4/cfgs_kw4x_3x_2x \
framework/XCVR/MKW36Z4 \
source \
source/common \
source/common/gatt_db \
startup \
utilities \

