################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/fsl_adc16.c \
../drivers/fsl_clock.c \
../drivers/fsl_common.c \
../drivers/fsl_dspi.c \
../drivers/fsl_flash.c \
../drivers/fsl_gpio.c \
../drivers/fsl_i2c.c \
../drivers/fsl_llwu.c \
../drivers/fsl_lptmr.c \
../drivers/fsl_lpuart.c \
../drivers/fsl_ltc.c \
../drivers/fsl_pmc.c \
../drivers/fsl_rtc.c \
../drivers/fsl_smc.c \
../drivers/fsl_tpm.c \
../drivers/fsl_trng.c 

OBJS += \
./drivers/fsl_adc16.o \
./drivers/fsl_clock.o \
./drivers/fsl_common.o \
./drivers/fsl_dspi.o \
./drivers/fsl_flash.o \
./drivers/fsl_gpio.o \
./drivers/fsl_i2c.o \
./drivers/fsl_llwu.o \
./drivers/fsl_lptmr.o \
./drivers/fsl_lpuart.o \
./drivers/fsl_ltc.o \
./drivers/fsl_pmc.o \
./drivers/fsl_rtc.o \
./drivers/fsl_smc.o \
./drivers/fsl_tpm.o \
./drivers/fsl_trng.o 

C_DEPS += \
./drivers/fsl_adc16.d \
./drivers/fsl_clock.d \
./drivers/fsl_common.d \
./drivers/fsl_dspi.d \
./drivers/fsl_flash.d \
./drivers/fsl_gpio.d \
./drivers/fsl_i2c.d \
./drivers/fsl_llwu.d \
./drivers/fsl_lptmr.d \
./drivers/fsl_lpuart.d \
./drivers/fsl_ltc.d \
./drivers/fsl_pmc.d \
./drivers/fsl_rtc.d \
./drivers/fsl_smc.d \
./drivers/fsl_tpm.d \
./drivers/fsl_trng.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/%.o: ../drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -D__REDLIB__ -DCPU_MKW36Z512VHT4_cm0plus -DCPU_MKW36Z512VHT4 -DDEBUG -DFRDM_KW36 -DFREEDOM -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\board" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\source" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\OSAbstraction\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\common" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\Flash\Internal" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\GPIO" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\Keyboard\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\TimersManager\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\TimersManager\Source" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\LED\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\MemManager\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\Lists" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\Messaging\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\Panic\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\RNG\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\NVM\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\NVM\Source" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\SerialManager\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\SerialManager\Source\I2C_Adapter" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\SerialManager\Source\SPI_Adapter" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\SerialManager\Source\UART_Adapter" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\ModuleInfo" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\FunctionLib" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\SecLib" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\host\interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\source\common" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\host\config" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\controller\interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\hci_transport\interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\source\common\gatt_db\macros" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\source\common\gatt_db" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\profiles\battery" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\profiles\device_info" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\bluetooth\profiles\hid" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\MWSCoexistence\Interface" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\drivers" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\CMSIS" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\utilities" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\DCDC\Interface\MKW36Z" -I"C:\Users\Matthijs\Documents\MCUXpressoIDE_11.1.0_3209\workspace\FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm\framework\XCVR\MKW36Z4" -O0 -fno-common -g -Wall -Wno-implicit-function-declaration  -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin -imacros "C:/Users/Matthijs/Documents/MCUXpressoIDE_11.1.0_3209/workspace/FreedomKW36HIDDeviceTest_wireless_examples_bluetooth_hid_device_bm/source/app_preinclude.h" -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m0plus -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


