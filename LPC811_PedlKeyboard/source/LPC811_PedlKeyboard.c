/*
 * Copyright 2016-2020 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    LPC811_PedlKeyboard.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC811.h"
/* TODO: insert other include files here. */
#include "fsl_gpio.h"

#define MAX_BUFFERED_ROTATIONS 10
volatile static int Rotations = 0;
/* TODO: insert other definitions and declarations here. */

void RotationClockwiseCallback(pint_pin_int_t pin, uint32_t wtf)
{
	// Get the channel B Level
	bool ChannelBLevel = GPIO_PinRead(GPIO, 0, 11);

	// If Channel B is read as High AND rotations is still in the max buffer rotations bounds...
	if(ChannelBLevel && Rotations > -MAX_BUFFERED_ROTATIONS)
		// ..decrement the rotations count
		Rotations--;
	// else if Channel B is read as Low AND rotations is still in the max buffer rotations bounds...
	else if (!ChannelBLevel && Rotations < MAX_BUFFERED_ROTATIONS)
		// ..increment the rotations count
		Rotations++;
}


/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();

    // Add the 'FSL_SDK_DISABLE_DRIVER_RESET_CONTROL' with value '1' to the projects 'Paths & Symbols' list to not have the GPIO Setup erased by the init function
    BOARD_InitBootPeripherals();
    //BOARD_InitBootPins();

    //printf("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
	GPIO_PinWrite(GPIO, 0, 8, 1);
	GPIO_PinWrite(GPIO, 0, 9, 1);

    while(1) {

    	if(i == 0 && Rotations != 0)
    	{
    		if(Rotations > 0)
    		{
    			GPIO_PinWrite(GPIO, 0, 9, 0);
    			Rotations--;
    		}
    		else
			{
    			GPIO_PinWrite(GPIO, 0, 8, 0);
    			Rotations++;
			}
    		i = 8000;
    	}
    	// If we have a value for i...
    	if(i != 0)
    		// ..count it down
    		i--;

    	// If we have reached the low count value for i...
    	if(i == 3000)
    	{
    		// ..Release the key(s)
        	GPIO_PinWrite(GPIO, 0, 8, 1);
        	GPIO_PinWrite(GPIO, 0, 9, 1);
    	}

    	/* 'Dummy' NOP to allow source level single stepping of
            tight while() loop */
        __asm volatile ("nop");
    }
    return 0 ;
}
